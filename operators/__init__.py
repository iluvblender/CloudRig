from . import flatten_chain
from . import toggle_metarig
from . import assign_bone_layers
from . import toggle_action_constraints
from . import edit_widget

modules = [
	flatten_chain
	,toggle_metarig
	,assign_bone_layers
	,toggle_action_constraints
	,edit_widget
]
